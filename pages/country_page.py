from locators.country_locators import CountryLocators
from parsers.country_parser import CountryParser
from selenium.webdriver.common.by import By

class CountryPage:
    def __init__(self, page):
        self.page = page

    def getData(self):
        content = self.page.find_element(By.CLASS_NAME, CountryLocators.mainContent)
        return {'name': CountryParser(content).getCountryName(),
                'figures': CountryParser(content).getFigures(),
                'time': CountryParser(content).getLastUpdatedDateAndTime()}

