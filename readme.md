# **COVID-19 TRACKER**


**To run this project:-** 

###### 1.clone this repository.

###### 2.navigate to the project folder using -> `cd covid-19`

###### 3.install required project dependencies using -> `pipenv install`

###### 4.run `app.py` in your terminal by -> `pipenv run python app.py`  or you can just simply run it from any editor or IDE of your choice 
             
