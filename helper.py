import json
import logging
import time
import coloredlogs
from selenium.common.exceptions import NoSuchElementException


def handleError(function):
    def handle(*parameters):
        func = ''
        try:
            func = function(*parameters)
        except NoSuchElementException:
            notifyUser(logging.WARNING, "\nCheck your internet connection and try again later....")
        except KeyboardInterrupt:
            notifyUser(logging.ERROR, "\n\nAn unwanted interrupt received from keyboard...\n")
            func = handle(*parameters)
        except KeyError:
            notifyUser(logging.ERROR, "\nYou have entered the wrong value....\nTry Again....\n")
            func = handle(*parameters)
        finally:
            return func
    return handle


def getLogger():
    logger = logging.getLogger(__name__)
    coloredlogs.install(fmt='%(message)s', level=logging.DEBUG, logger=logger)
    return logger


def notifyUser(method, message):
    if method == logging.WARNING:
        getLogger().warning(message)
    elif method == logging.ERROR:
        getLogger().error(message)
    elif method == logging.CRITICAL:
        getLogger().critical(message)
    else:
        getLogger().info(message)
    time.sleep(0)

@handleError
def getUserCountryInput():
    with open('countries.json', 'r') as json_file:
        country_choice = json.load(json_file)[input("Enter country name: ").upper()]
    return country_choice


@handleError
def isContinuing():
    user_choice = input("Do you want to continue (Y/N) : ").upper()
    if user_choice == 'Y':
        print()
        return True
    else:
        return False

