from cachetools import cached, TTLCache
from selenium import webdriver

from helper import getUserCountryInput, isContinuing, handleError
from pages.country_page import CountryPage


def getChromeInstance():
    driver_options = webdriver.ChromeOptions()
    driver_options.add_argument("headless")
    chrome = webdriver.Chrome(executable_path='/usr/local/bin/chromedriver', options=driver_options)
    return chrome


@handleError
@cached(cache=TTLCache(maxsize=2, ttl=600))
def getFinalReport(country_choice):
    instance = getChromeInstance()
    instance.get(f'https://www.worldometers.info/coronavirus/country/{country_choice}/')
    data = CountryPage(instance).getData()
    if len(data.keys()) == 3:
        instance.close()
    country_name = data['name']
    figures = data['figures']
    time = data['time']
    item = f'''
<==========================================>
 {country_name}

 {figures[0]}
 {figures[1]}
 {figures[2]}


 As of {time}
<==========================================>
'''
    return item


def performAction():
    choice = getUserCountryInput()
    if choice:
        print(getFinalReport(choice))


def initialise(start):
    if start:
        print('\n/<<<<<<<<<<<<<<<<<<<<<<<<<<<| COVID-19 TRACKER |>>>>>>>>>>>>>>>>>>>>>>>>>>>\ \n')
        performAction()
    ready = isContinuing()
    if ready:
        performAction()
        initialise(False)
    else:
        print('\n\<<<<<<<<<<<<<<<<<<<<<<<<<<<| COVID-19 TRACKER |>>>>>>>>>>>>>>>>>>>>>>>>>>>/\n')


initialise(True)
