from locators.figures_locator import FiguresLocators
from selenium.webdriver.common.by import By
import calendar
import time

class CountryParser:
    def __init__(self, page):
        self.page = page

    def getCountryName(self):
        locator = FiguresLocators.name
        return self.page.find_element(By.ID, locator).text.split(' / ')[-1]

    def getFigures(self):
        locator = FiguresLocators.figures
        return [item.text.replace('\n', ' ') for item in self.page.find_elements(By.ID, locator)]

    def getLastUpdatedDateAndTime(self):
        string = [item.text for item in self.page.find_elements(By.TAG_NAME, 'div') if 'Last updated:' in item.text][0]
        string = string.replace('Last updated: ', '')
        time_tuple = time.strptime(string, "%B %d, %Y, %H:%M GMT")
        t = calendar.timegm(time_tuple)
        return time.strftime("%d %B, %Y | %I:%M %p IST", time.localtime(t))
